﻿using Colegio.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Cors;

namespace Colegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("permitir")]
    public class EstudiantesController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                var lst = (from d in db.Estudiantes
                           select d).ToList();
                return Ok(lst);
            }
        }


        [HttpGet("{id}", Name = "getById")]
        public ActionResult getById(Estudiante model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {

                return Ok(db.Estudiantes.Find(model.Id));
            }
        }

        [HttpPost]
        [ResponseType(typeof(Estudiante))]
        public ActionResult POST([FromBody] Models.Request.EstudiantesRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Estudiante oEstudiante = new Models.Estudiante();
                oEstudiante.Nombres = model.Nombres;
                oEstudiante.Apellidos = model.Apellidos;
                oEstudiante.Documento = model.Documento;
                oEstudiante.TipoDocumento = model.Tipo_Documento;
                oEstudiante.Estado =1;
                db.Estudiantes.Add(oEstudiante);
                db.SaveChanges();
                return CreatedAtRoute("getById", new { Id = oEstudiante.Id }, oEstudiante);
            }
        }

        [HttpPut]
        public ActionResult PUT([FromBody] Models.Request.EstudiantesEditRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Estudiante oEstudiante = db.Estudiantes.Find(model.id);
                oEstudiante.Nombres = model.Nombres;
                oEstudiante.Apellidos = model.Apellidos;
                oEstudiante.TipoDocumento = model.Tipo_Documento;
                oEstudiante.Estado = (byte?)model.Estado;
                oEstudiante.Documento = model.Documento;
                db.Entry(oEstudiante).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult DELETE(int id)
        {
            /* Algo */
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Estudiante oEstudiante = db.Estudiantes.Find(id);
                db.Estudiantes.Remove(oEstudiante);
                db.SaveChanges();
                return Ok(oEstudiante);
            }
        }
    }
}
