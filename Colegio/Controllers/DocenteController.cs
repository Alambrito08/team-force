﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Colegio.Models;
using System.Web.Http.Description;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Cors;

namespace Colegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("permitir")]
    public class DocenteController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                var lst = (from d in db.Docentes
                           select d).ToList();
                return Ok(lst);
            }
        }

        public ActionResult getById(Docente model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {

                return Ok(db.Docentes.Find(model.id));
            }
        }

        [HttpPost]
        [ResponseType(typeof(Docente))]
        public ActionResult POST([FromBody] Models.Request.DocenteRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Docente oDocente = new Models.Docente();
                oDocente.Nombres = model.Nombres;
                oDocente.Apellidos = model.Apellidos;
                oDocente.Documento = model.Documento;
                oDocente.TipoDocumento = model.TipoDocumento;
                oDocente.Estado = model.Estado;
                db.Docentes.Add(oDocente);
                db.SaveChanges();
                return CreatedAtRoute("getById", new { Id = oDocente.id }, oDocente);
            }
        }
        [HttpPut]
        public ActionResult PUT([FromBody] Models.Request.DocenteEditRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Docente oDocente = db.Docentes.Find(model.id);
                oDocente.Nombres = model.Nombres;
                oDocente.Apellidos = model.Apellidos;
                oDocente.TipoDocumento = model.TipoDocumento;
                oDocente.Estado = (byte?)model.Estado;
                oDocente.Documento = model.Documento;
                db.Entry(oDocente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult DELETE(int id)
        {
            /* Algo */
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Docente oDocente = db.Docentes.Find(id);
                db.Docentes.Remove(oDocente);
                db.SaveChanges();
                return Ok(oDocente);
            }
        }

    }

}
