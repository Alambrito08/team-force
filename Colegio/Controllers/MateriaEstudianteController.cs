﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Colegio.Models;
using System.Web.Http.Description;
using Microsoft.AspNetCore;

namespace Colegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriaEstudianteController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                var lst = (from d in db.MateriaEstudiantes
                           select d).ToList();
                return Ok(lst);
            }
        }

        public ActionResult getById(Docente model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {

                return Ok(db.MateriaEstudiantes.Find(model.id));
            }
        }

        [HttpPost]
        [ResponseType(typeof(MateriaEstudiante))]
        public ActionResult POST([FromBody] Models.Request.MateriaEstudianteRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.MateriaEstudiante oMateriaEstudiante = new Models.MateriaEstudiante();
                oMateriaEstudiante.Nota1 = model.Nota1;
                oMateriaEstudiante.Nota2 = model.Nota2;
                oMateriaEstudiante.MateriaId = model.MateriaId;
                oMateriaEstudiante.EstudianteId = model.EstudianteId;

                db.MateriaEstudiantes.Add(oMateriaEstudiante);
                db.SaveChanges();
                return CreatedAtRoute("getById", new { Id = oMateriaEstudiante.Id }, oMateriaEstudiante);
            }
        }
        [HttpPut]
        public ActionResult PUT([FromBody] Models.Request.MateriaEstudianteEditRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.MateriaEstudiante oMateriaEstudiante = db.MateriaEstudiantes.Find(model.Id);
                oMateriaEstudiante.Nota1 = model.Nota1;
                oMateriaEstudiante.Nota2 = model.Nota2;
                oMateriaEstudiante.MateriaId = model.MateriaId;
                oMateriaEstudiante.EstudianteId = model.EstudianteId;
                db.Entry(oMateriaEstudiante).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult DELETE(int id)
        {
            /* Algo */
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.MateriaEstudiante oMateriaEstudiante = db.MateriaEstudiantes.Find(id);
                db.MateriaEstudiantes.Remove(oMateriaEstudiante);
                db.SaveChanges();
                return Ok(oMateriaEstudiante);
            }
        }


    }
}
