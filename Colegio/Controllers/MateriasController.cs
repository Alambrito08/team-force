﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Colegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriasController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                var lst = (from d in db.Materia 
                           join a in db.Docentes on d.DocenteId equals a.id
                           select new { NombreMateria = d.Nombre, NombreDocente = a.Nombres,ApellidoDocente = a.Apellidos }).ToList();
                return Ok(lst);
            }
        }
        [HttpPost]
        public ActionResult Post([FromBody] Models.Request.MateriasRequest model)
        {
            using(Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Materia oMateria = new Models.Materia();
                oMateria.Nombre = model.Nombre;
                oMateria.DocenteId = model.Docente_id;
                db.Materia.Add(oMateria);
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpPut]
        public ActionResult PUT([FromBody] Models.Request.MateriasEditRequest model)
        {
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Materia oMateria = db.Materia.Find(model.Id);
                oMateria.Nombre = model.Nombre;
                oMateria.DocenteId = model.Docente_id;
                db.Entry(oMateria).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult DELETE(int id)
        {
            /* Algo */
            using (Models.ColegioContext db = new Models.ColegioContext())
            {
                Models.Materia oMateria = db.Materia.Find(id);
                db.Materia.Remove(oMateria);
                db.SaveChanges();
                return Ok(oMateria);
            }
        }
    }
    }

