﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Colegio.Models
{
    public partial class Estudiante
    {
        public Estudiante()
        {
            MateriaEstudiantes = new HashSet<MateriaEstudiante>();
        }

        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Documento { get; set; }
        public string TipoDocumento { get; set; }
        public byte? Estado { get; set; }

        public virtual ICollection<MateriaEstudiante> MateriaEstudiantes { get; set; }
    }
}
