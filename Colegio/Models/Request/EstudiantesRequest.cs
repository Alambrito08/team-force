﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Colegio.Models.Request
{
    public class EstudiantesRequest
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Documento { get; set; }
        public string Tipo_Documento { get; set; }
        public byte? Estado { get; set; }
        
    }
}
