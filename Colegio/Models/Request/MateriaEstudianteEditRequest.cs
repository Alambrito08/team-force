﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Colegio.Models.Request
{
    public class MateriaEstudianteEditRequest
    {
        public int Id { get; set; }
        public double? Nota1 { get; set; }
        public double? Nota2 { get; set; }
        public int MateriaId { get; set; }
        public int EstudianteId { get; set; }

        
    }
}
