﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Colegio.Models.Request
{
    public class MateriaEstudianteRequest
    {
        public double? Nota1 { get; set; }
        public double? Nota2 { get; set; }
        public int MateriaId { get; set; }
        public int EstudianteId { get; set; }

       
    }
}
