﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Colegio.Models.Request
{
    public class MateriasRequest
    {
        public string Nombre { get; set; }
        public int Docente_id { get; set; }
    }
}
