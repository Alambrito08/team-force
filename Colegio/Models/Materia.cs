﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Colegio.Models
{
    public partial class Materia
    {
        public Materia()
        {
            MateriaEstudiantes = new HashSet<MateriaEstudiante>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int DocenteId { get; set; }

        public virtual Docente Docente { get; set; }
        public virtual ICollection<MateriaEstudiante> MateriaEstudiantes { get; set; }
    }
}
