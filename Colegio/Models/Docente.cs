﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Colegio.Models
{
    public partial class Docente
    {
        public Docente()
        {
            Materia = new HashSet<Materia>();
        }

        public int id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        
        public string TipoDocumento { get; set; }
        public byte? Estado { get; set; }
        public string Documento { get; set; }

        public virtual ICollection<Materia> Materia { get; set; }
    }
}
