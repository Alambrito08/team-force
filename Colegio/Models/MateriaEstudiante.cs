﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Colegio.Models
{
    public partial class MateriaEstudiante
    {
        public int Id { get; set; }
        public double? Nota1 { get; set; }
        public double? Nota2 { get; set; }
        public int MateriaId { get; set; }
        public int EstudianteId { get; set; }

        public virtual Estudiante Estudiante { get; set; }
        public virtual Materia Materia { get; set; }
    }
}
